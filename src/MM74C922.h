#pragma once
#include <cstdint>
#include "ATMega.h"

class MM74C922
{
	public:
		MM74C922( Port com );
		uint8_t lineReady( void );
		int getline( char* string, uint8_t chars );
};

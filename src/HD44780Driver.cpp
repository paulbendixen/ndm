#include "HD44780Driver.h"
#include <avr/io.h>

// The driver has the following signals:
// RS	MOSI Selects Registers: 0 Instuction register / busy flag; 1 data
// R/\W	MOSI Selects 0: Write; 1: Read
// E	MOSI Enable, start data read write
//
// DB7	BIRIR ( Possible busy flag )
// ...	BIDIR Data or registers, read or write, 4-bit operation on MSB
// DB0	

HD44780Driver::HD44780Driver( Port dataPort, Port controlPort )
	:myDataPort( dataPort ), myControlPort( controlPort )
{
}

void HD44780Driver::clearDisplay( void )
{
	writeCommand( static_cast< char >( Command::clearDisplay ) );
}

/**
 * Wait (eternaly?) for the busy flag to be cleared.
 * The function is implemented as a busy wait routine, so this will probably eat up some resources
 */
void HD44780Driver::waitWhileBusy( void )
{
	uint8_t busyflag;
	do
	{
		busyflag = readReadyAddress();
	}
	while( ( busyflag & 0x80 ) != 0x00 );
}

/**
 * Go to the given address on the display.
 * Please remark that the addressing on the display is from top left,
 * so that in order to go to the bottom rigt, some calculations must be done.
 * ATM it is not considered neccesary to use a lot of energy to make the display
 * a certain size, but it might become handy sometime. YAGNI
 */
void HD44780Driver::gotoAddress( uint8_t address )
{
	address |= static_cast< uint8_t >( Command::setDDRAM );
	writeCommand( address );
}

unsigned char HD44780Driver::readAddress( void )
{
	uint8_t address;
	address = readReadyAddress();
	return address & 0x7F;
}

void HD44780Driver::writeCommand( uint8_t command )
{
}

uint8_t HD44780Driver::readReadyAddress( void )
{
}

/**
 * This command blanks the display or unblanks it.
 * There is no need right now to implement cursor or blinking, so YAGNI
 */
void HD44780Driver::blankDisplay( bool blank )
{
	auto myCommand = static_cast< uint8_t >( Command::displayOnOff );
	if ( !blank )
	{
		myCommand |= 0x04;
	}
	writeCommand( myCommand );
}

void HD44780Driver::printChar( char)
{
}
uint8_t HD44780Driver::readChar( void )
{
}

#include "HD44780.h"

HD44780::HD44780( Port dataPort, Port controlPort )
	:driver( dataPort, controlPort )
{
}
		
void HD44780::clearScreen( void )
{
	driver.clearDisplay();
}

HD44780& HD44780::operator<<( uint8_t number )
{
	return *this;
}

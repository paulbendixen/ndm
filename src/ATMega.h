#pragma once

enum class Port
{
	PortA,
	PortB,
	PortC,
	PortD,
	PortE,
	PortF
};

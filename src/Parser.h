#pragma once
#include <cstdint>

class Parser
{
	struct Parameters
	{
		uint8_t numberOfDice;
		uint8_t dieType;
		uint8_t multiplier;
		uint8_t addition
	};

	Parameters roll( char* rollString );
};

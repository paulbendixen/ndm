#pragma once
#include <cstdint>
#include "ATMega.h"
#include "HD44780Driver.h"

class HD44780
{
	public:
		explicit HD44780( Port dataPort = Port::PortA, Port controlPort = Port::PortB );
		
		void clearScreen( void );
		HD44780& operator<<( uint8_t number );
		HD44780& operator<<( char* string );
	private:
		HD44780Driver driver;
};

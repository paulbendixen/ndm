#pragma once
#include <cstdint>
#include "ATMega.h"

class HD44780Driver
{
	public:
		HD44780Driver( Port dataPort, Port controlPort );
		void waitWhileBusy( void );
		void printChar( char );
		uint8_t readChar( void );
		void clearDisplay( void );
		void blankDisplay( bool blank );
		void gotoAddress( uint8_t address );
		uint8_t readAddress( void );
	private:
		enum class Command : uint8_t 
		{
			clearDisplay = 0x01,
			returnHome = 0x02,
			entryModeSet = 0x04,
			displayOnOff = 0x08,
			cursorDisplayShift = 0x10,
			functionSet = 0x20,
			setCGRAM = 0x40,
			setDDRAM = 0x80
		};
		Port myControlPort;
		Port myDataPort;
		void writeCommand( uint8_t command );
		uint8_t readReadyAddress( void );

};



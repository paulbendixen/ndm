TARGET = ndm

SRC = ndm.cpp

CC = avr-gcc
CXX = avr-g++

OBJ = $(SRC:.cpp:.o)

CFLAGS=-g -O2 -Wall -mcall-prologues -mmcu=atmega32
CXXFLAGS=-g -O2 -Wall -mcall-prologues -mmcu=atmega32 std=c++11

AVRFLAGS= -p m32 -P usb -c avrispmkII -e -u -U $(TARGET).hex -v -B 10
OBJ2HEX=/usr/bin/avr-objcopy 
UISP=/usr/bin/avrdude 

BUILDDIR = build
SRCDIR = src
OUTPUTDIR = exe

%.o: %.c
	$(CC) -c $(SRCDIR)/$* $(CFLAGS) -o $(BUILDDIR)/$^

%.o: %.cpp
	$(CXX) -c $(SRCDIR)/$* $(CXXFLAGS) -o $(BUILDDIR)/$*.c

.SECONDARY : $(TARGET).hex
.PRECIOUS :$(OBJ)

$(BUILDDIR)/%.obj : $(prefix $(BUILDDIR)/, $(OBJ) )
	$(CC) $(CFLAGS) $(OBJ) -o $@

$(OUTPUTDIR)/%.hex : $(BUILDDIR)/%.obj
	$(OBJ2HEX) -R .eeprom -O ihex $< $@


program : $(OUTPUTDIR)/$(TARGET).hex
	$(UISP) $(AVRFLAGS) 

clean :
	rm -f *.hex *.obj *.o

all: $(OUTPUTDIR)/$(TARGET).hex

